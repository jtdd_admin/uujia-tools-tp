<?php

namespace utils\tools;

/**
 * Class NumberTool
 * Date: 2021/4/5
 * Time: 18:12
 *
 * 号码工具
 *
 * @package utils\tools
 */
class NumberTool {
	
	/**
	 * 构建号码 根据首尾号（返回迭代器）
	 * 例如：first=10001 last=10005 result=[10001, 10002, 10003, 10004, 10005]
	 *
	 * Date: 2021/4/5
	 * Time: 18:08
	 *
	 * @param $first
	 * @param $last
	 * @return \Generator
	 */
	public static function makeByFirstLast($first, $last) {
		for ($i = $first; $i <= $last; $i++) {
			yield number_format($i, 0, '', '');
		}
	}
	
	/**
	 * 构建号码 根据首尾号（返回数组）
	 * 例如：first=10001 last=10005 result=[10001, 10002, 10003, 10004, 10005]
	 *
	 * Date: 2021/4/5
	 * Time: 18:11
	 *
	 * @param $first
	 * @param $last
	 * @return array
	 */
	public static function makeArrByFirstLast($first, $last) {
		$arr = [];
		foreach (self::makeByFirstLast($first, $last) as $n) {
			$arr[] = $n;
		}
		
		return $arr;
	}
	
}