<?php


namespace utils\tools;


class QpsTool {
	
	/**
	 * Redis对象
	 *
	 * @var \Redis
	 */
	protected $redis;
	
	/**
	 * 前缀
	 *
	 * @var string
	 */
	protected $prefix = '';
	
	/**
	 * QpsTool constructor.
	 * @param \Redis $redis
	 * @param string $prefix
	 */
	public function __construct(\Redis $redis, string $prefix = '') {
		$this->redis = $redis;
		$this->prefix = $prefix;
	}
	
	/**
	 * qps限制检查
	 *
	 * @param $method
	 * @param int $max
	 * @param int $time
	 * @return bool
	 */
	public function checkQps($method, $max = 500, $time = 60) {
		$key = $this->prefix . 'qps:' . $method;
		
		// 判断key是否存在
		$check = $this->redis->exists($key);
		if ($check){
			// +1
			$this->redis->incr($key);
			$count = $this->redis->get($key);
			// 限制每分钟 max 次
			if($count >= $max){
				return false;
			}
		} else {
			$this->redis->set($key, 1);//初始值1
			$this->redis->expire($key, $time);//有效期time
		}
		
		return true;
	}
	
}