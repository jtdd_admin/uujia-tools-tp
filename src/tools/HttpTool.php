<?php


namespace utils\tools;

use GuzzleHttp\Client;

/**
 * Class HttpTool
 * Date: 2021/4/5
 * Time: 21:22
 *
 * @package utils\tools
 */
class HttpTool {
	
	/**
	 * @var null|Client
	 */
	public static $clientGuzzleHttp = null;
	
	/**
	 * http并发池
	 *
	 * @param \Closure $requests
	 * @return array
	 */
	public static function httpPool(\Closure $requests) {
		// $requests = function() use ($comm_ids) {
		// 	$base_url = APF::get_instance()->get_config('broker_basic_url');
		// 	foreach ($comm_ids as $comm_id) {
		// 		$url = $base_url . 'broker/commSignTopSigner/?is_nocheck=1&commId=' . $comm_id;
		// 		yield new GuzzleHttp\Psr7\Request('GET', $url);
		// 	}
		// };
		
		$responses = [];
		$pool = new \GuzzleHttp\Pool(new \GuzzleHttp\Client(), $requests(), [
			'concurrency' => 5,
			'fulfilled' => function ($response, $index) use (&$responses) {
				$responses[$index] = $response;
			},
			'rejected' => function ($reason, $index) use (&$responses) {
				$responses[$index] = [];
			},
		]);
		
		$pool->promise()->wait();

		// // 输出response集合
		// foreach ($responses as $response) {
		// 	$response->getBody()->getContents()
		// }
		
		$client = new \GuzzleHttp\Client();
		
		// $requests = function ($total) use ($client) {
		// 	for ($i = 0; $i < $total; $i++) {
		// 		$url = "domain.com/picture/{$i}.jpg";
		// 		$filepath = "/tmp/{$i}.jpg";
		//
		// 		yield function($poolOpts) use ($client, $url, $filepath) {
		// 			/** Apply options as follows:
		// 			 * Client() defaults are given the lowest priority
		// 			 * (they're used for any values you don't specify on
		// 			 * the request or the pool). The Pool() "options"
		// 			 * override the Client defaults. And the per-request
		// 			 * options ($reqOpts) override everything (both the
		// 			 * Pool and the Client defaults).
		// 			 * In short: Per-Request > Pool Defaults > Client Defaults.
		// 			 */
		// 			$reqOpts = [
		// 				'sink' => $filepath
		// 			];
		// 			if (is_array($poolOpts) && count($poolOpts) > 0) {
		// 				$reqOpts = array_merge($poolOpts, $reqOpts); // req > pool
		// 			}
		//
		// 			return $client->getAsync($url, $reqOpts);
		// 		};
		// 	}
		// };
		//
		// $pool = new Pool($client, $requests(100));
		
		return $responses;
	}

    /**
     * httpGet
     *
     * Date: 2021/4/10
     * Time: 19:38
     *
     * @param       $url
     * @param array $params
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function httpGet($url, $params = []) {
        // Create a client with a base URI
        empty(self::$clientGuzzleHttp) && self::$clientGuzzleHttp = new \GuzzleHttp\Client();//['base_uri' => $url]
        // Send a request to URI
//        $response = self::$clientGuzzleHttp->request('GET', $url, ['body' => http_build_query($params)]);
        $response = self::$clientGuzzleHttp->request('GET', $url, ['body' => http_build_query($params)]);
//        $response = self::$clientGuzzleHttp->request('GET', $url, ['form_params' => $params]);

        return $response;
    }

	/**
	 * httpPost
	 *
	 * Date: 2021/4/10
	 * Time: 19:38
	 *
	 * @param       $url
	 * @param array $params
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function httpPost($url, $params = []) {
		// Create a client with a base URI
		empty(self::$clientGuzzleHttp) && self::$clientGuzzleHttp = new \GuzzleHttp\Client();//['base_uri' => $url]
		// Send a request to URI
		// $response = self::$clientGuzzleHttp->request('POST', $url, ['body' => http_build_query($params)]);
		$response = self::$clientGuzzleHttp->request('POST', $url, ['form_params' => $params]);
		
		return $response;
	}
	
	/**
	 * httpPostAsync
	 *
	 * Date: 2021/4/10
	 * Time: 19:38
	 *
	 * @param       $url
	 * @param array $params
	 * @return \GuzzleHttp\Promise\PromiseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function httpPostAsync($url, $params = []) {
		// Create a client with a base URI
		empty(self::$clientGuzzleHttp) && self::$clientGuzzleHttp = new \GuzzleHttp\Client();
		// Send a request to URI
		$promise = self::$clientGuzzleHttp->requestAsync('POST', $url, ['form_params' => $params]);
		
		return $promise;
	}

	
	/**
	 * 说明：获取完整URL
	 * @return string
	 */
	public static function curPageURL() {
		$pageURL = 'http';
		
		if ($_SERVER["HTTPS"] == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
		}
		else {
			$pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
		}
		
		return $pageURL;
	}
	
	/**
	 * 获取URL头部
	 */
	public static function curPageURL_head() {
		$pageURL = 'http';
		
		if ($_SERVER["HTTPS"] == "on") {
			$pageURL .= "s";
		}
		$pageURL .= "://";
		
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
		}
		else {
			$pageURL .= $_SERVER["SERVER_NAME"];
		}
		
		return $pageURL;
	}
	
	/**
	 * GET 请求
	 * @param string $url
	 * @return bool|mixed
	 */
	public static function http_get($url){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
	
	/**
	 * POST 请求
	 * @param string $url
	 * @param array $param
	 * @param boolean $post_file 是否文件上传
	 * @return string content
	 */
	public static function http_post($url, $param, $post_file = false) {
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
		}
		if (is_array($param) || $post_file) {
			if(version_compare("5.5",PHP_VERSION,'<=')){   //当前版本大于5.5
				foreach($param as $param_key=>&$param_row){
					if(is_string($param_row)){
						if($param_row[0]=='@'){
							$param_row=new CURLFile(substr($param_row,1));
						}
					}else if(is_array($param_row)){
						if($param_row[0][0]=='@'){
							$param_row=new CURLFile(substr($param_row[0],1),$param_row[1],$param_row[2]);
						}
					}
				}
			}
		}
		$strPOST = $param;
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($oCurl, CURLOPT_POST,true);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS, $strPOST);
		curl_setopt($oCurl,CURLOPT_FOLLOWLOCATION,true);
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
	
	/**
	 * POST 请求xml
	 * @param string $url
	 * @param array $xml   直接xml字符传来
	 * @param boolean $post_file 是否文件上传
	 * @return string content
	 */
	public static function http_post_xml($url, $xml, $post_file = false) {
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
		}
		
		$strPOST = $xml;
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($oCurl, CURLOPT_POST,true);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
		curl_setopt($oCurl,CURLOPT_FOLLOWLOCATION,true);
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
	
	
}