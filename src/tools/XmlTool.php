<?php


namespace utils\tools;

/**
 * Class XmlTool
 * Date: 2021/4/5
 * Time: 18:19
 *
 * @package utils\tools
 */
class XmlTool {
	
	/**
	 * XML转数组
	 *
	 * Date: 2021/4/5
	 * Time: 18:24
	 *
	 * @param $xml
	 * @return array|mixed
	 */
	public static function xml2arr($xml) {
		if (empty($xml)) {
			return [];
		}
		
		$result = [];
		
		$xmlobj = self::isimplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
		
		//if($xmlobj instanceof SimpleXMLElement) {
		$result = json_decode(json_encode($xmlobj), true);
		
		if (is_array($result)) {
			return $result;
		} else {
			return [];
		}
		// } else {
		// 	return $result;
		// }
	}
	
	/**
	 * 自定义xml验证函数xml_parser()
	 *
	 * @param $str
	 * @return false|mixed
	 */
	public static function xml_parser($str){
		$xml_parser = xml_parser_create();
		if(!xml_parse($xml_parser,$str,true)){
			xml_parser_free($xml_parser);
			return false;
		}else {
			return (json_decode(json_encode(simplexml_load_string($str)),true));
		}
	}
	
	/**
	 * 数组转XML
	 *
	 * 对于value两种情况 其中一种是一般字符 可生成<key>value</key>
	 * [
	 *      [key] => 'value',
	 * ]
	 *
	 * 另一种为数组 可生成<key><k1 param1="111">value1</k1><k2>value2</k2></key>
	 * [
	 *      [key] => [
	 *          [k1] => [
	 *              '__param__' => [
	 *                  param1 => '111',
	 *                  b => '',
	 *              ]
	 *          ],
	 *          [key2] => 'value2',
	 *          '__value__' => 'value2', // 只存在__param__时才能有__value__ 否则内部存在子key value就不可能存在自己value
	 *      ],
	 *      '__param__' => [
	 *         a => '',
	 *         b => '',
	 *      ]
	 * ]
	 *
	 * Date: 2021/4/5
	 * Time: 18:25
	 *
	 * @param $arr
	 * @return string
	 */
	public static function arr2Xml($arr) {
		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= self::arr2Xml_data($arr, '');
		
		return $xml;
	}
	
	/**
	 * arr2Xml_data
	 *
	 * Date: 2021/4/5
	 * Time: 18:33
	 *
	 * @param        $arr
	 * @param string $key
	 * @return string
	 */
	protected static function arr2Xml_data($arr, $key = '') {
		if (empty($arr)) {
			return '';
		}
		
		$xml    = '';
		$_kv    = '';
		$_value = '';
		$_param = '';
		
		if (isset($arr['__value__'])) {
			if (!empty($key)) {
				if (is_array($arr['__value__'])) {
					$_value = self::arr2Xml_data($arr['__value__'], $key);
				} elseif (is_numeric($arr['__value__'])) {
					$_value = $arr['__value__'];
				}
			}
			
			unset($arr['__value__']);
		}
		
		if (isset($arr['__param__'])) {
			foreach ($arr['__param__'] as $k => $v) {
				$_param .= " {$k}=\"{$v}\"";
			}
			
			unset($arr['__param__']);
		}
		
		// arr是数组 遍历每一个元素
		foreach ($arr as $k => $value) {
			if (is_array($value)) {
				$_kv .= self::arr2Xml_data($value, $k);
			} elseif (is_numeric($value)) {
				$_kv .= "<{$k}>{$value}</{$k}>";
			} else {
				//$_kv .= "<{$k}><![CDATA[{$value}]]></{$k}>";
				$_kv .= "<{$k}>{$value}</{$k}>";
			}
		}
		
		if (!empty($key)) {
			if (!empty($_kv)) {
				$xml .= "<{$key}{$_param}>{$_kv}</{$key}>";
			} else {
				$xml .= "<{$key}{$_param}>{$_value}</{$key}>";
			}
		} else {
			$xml .= $_kv;
		}
		
		return $xml;
	}
	
	protected static function isimplexml_load_string($string, $class_name = 'SimpleXMLElement', $options = 0, $ns = '', $is_prefix = false) {
		libxml_disable_entity_loader(true);
		
		if (preg_match('/(\<\!DOCTYPE|\<\!ENTITY)/i', $string)) {
			return false;
		}
		
		return simplexml_load_string($string, $class_name, $options, $ns, $is_prefix);
	}
	
	/**
	 * xml转换 array
	 *
	 * @param $xml
	 * @return array
	 */
	public static function xml_to_array($xml) {
		$reg = "/<(\\w+)[^>]*?>([\\x00-\\xFF]*?)<\\/\\1>/";
		if (preg_match_all($reg, $xml, $matches)) {
			$count = count($matches[0]);
			$arr   = array();
			for ($i = 0; $i < $count; $i++) {
				$key = $matches[1][$i];
				$val = self::xml_to_array($matches[2][$i]);  // 递归
				if (array_key_exists($key, $arr)) {
					if (is_array($arr[$key])) {
						if (!array_key_exists(0, $arr[$key])) {
							$arr[$key] = array($arr[$key]);
						}
					} else {
						$arr[$key] = array($arr[$key]);
					}
					$arr[$key][] = $val;
				} else {
					$arr[$key] = $val;
				}
			}
			
			return $arr;
		} else {
			return $xml;
		}
	}
	
	/**
	 * 数组转XML
	 *
	 * @param $array
	 * @return string
	 * @throws \think\Exception
	 */
	public static function array_to_xml($array) {
		if (!is_array($array)) {
			throw new \think\Exception("参数不是数组！");
		}
		
		$xml = "<xml>";
		foreach ($array as $key => $val) {
			if (is_numeric($val)) {
				$xml .= "<" . $key . ">" . $val . "</" . $key . ">";
			} else {
				$xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
			}
		}
		$xml .= "</xml>";
		
		return $xml;
	}
	
	
	public static function xml_decode($xml) {
		$re = [];
		if(!$xml) {
			return $re;
		}else{
			$xml_parser = xml_parser_create();
			if(!xml_parse($xml_parser,$xml,true)){
				xml_parser_free($xml_parser);
				return $xml;
			}else {
				libxml_disable_entity_loader(true);
				$re = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
				return $re;
			}
		}
	}
	
}