<?php

namespace utils\tools;

/**
 * Class TimeTool
 *
 * @package utils\tools
 */
class TimeTool {

    /**
     * 过去的时间 文字表示
     * 刚刚 1分钟之前
     *
     * @param $posttime
     *
     * @return mixed|string
     */
    public static function timeAgo($posttime) {
        //当前时间的时间戳
        $nowtimes = strtotime(date('Y-m-d H:i:s'), time());

        //之前时间参数的时间戳
        if (is_string($posttime)) {
            $posttimes = strtotime($posttime);
        } else {
            $posttimes = $posttime;
        }

        //相差时间戳
        $counttime = $nowtimes - $posttimes;

        //进行时间转换
        if ($counttime <= 10) {
            return '刚刚';
        } elseif ($counttime > 10 && $counttime <= 30) {
            return '刚才';
        } elseif ($counttime > 30 && $counttime <= 60) {
            return '刚一会';
        } elseif ($counttime > 60 && $counttime <= 120) {
            return '1分钟前';
        } elseif ($counttime > 120 && $counttime <= 180) {
            return '2分钟前';
        } elseif ($counttime > 180 && $counttime < 3600) {
            return intval(($counttime / 60)) . '分钟前';
        } elseif ($counttime >= 3600 && $counttime < 3600 * 24) {
            return intval(($counttime / 3600)) . '小时前';
        } elseif ($counttime >= 3600 * 24 && $counttime < 3600 * 24 * 2) {
            return '昨天';
        } elseif ($counttime >= 3600 * 24 * 2 && $counttime < 3600 * 24 * 3) {
            return '前天';
        } elseif ($counttime >= 3600 * 24 * 3 && $counttime <= 3600 * 24 * 20) {
            return intval(($counttime / (3600 * 24))) . '天前';
        } else {
            return $posttime;
        }
    }


}