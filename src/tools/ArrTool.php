<?php

namespace utils\tools;


use utils\traits\InstanceTrait;

class ArrTool {
	use InstanceTrait;
	
	protected $_arr;
	
	/**
	 * @param array $arr
	 * @return ArrTool
	 */
	public static function from(array &$arr) {
		/** @var ArrTool $me */
		$me = static::getInstance();
		$me->setArr($arr);
		
		return $me;
	}
	
	public function first() {
		return $this->_arr[0] ?? null;
	}
	
	/**
	 * last
	 *
	 * @return mixed
	 */
	public function last() {
		return $this->_arr[count($this->_arr) - 1] ?? null;
	}
	
	/**
	 * @return $this
	 */
	public function getArr() {
		return $this->_arr;
	}
	
	/**
	 * @param array $arr
	 * @return $this
	 */
	public function setArr(array &$arr) {
		$this->_arr = $arr;
		
		return $this;
	}
	
	/**
	 * 数组转字符串
	 *
	 * @param array  $arr
	 * @param string $glue
	 *
	 * @return string
	 */
	public static function arrToStr(array $arr, string $glue = ',') {
		return implode($glue, $arr);
	}
	
	/**
	 * 字符串转数组
	 *
	 * @param string   $str
	 * @param string   $delimiter
	 *
	 * @param int|null $limit
	 *
	 * @return array
	 */
	public static function strToArr(string $str, string $delimiter = ',', int $limit = null) {
		return explode($delimiter, $str);
	}
	
	/**
	 * 不重复追加
	 *
	 * Date: 2021/2/22
	 * Time: 18:45
	 *
	 * @param array            $arr
	 * @param int|string|array $value
	 * @return array
	 */
	public static function pushUnique(array $arr, $value) {
		if (is_array($value)) {
			foreach ($value as $item) {
				if (!in_array($item, $arr)) {
					array_push($arr, $item);
				}
			}
		} else {
			if (!in_array($value, $arr)) {
				array_push($arr, $value);
			}
		}
		
		return $arr;
	}
	
	/**
	 * 计算多个集合的笛卡尔积
	 *
	 * Date: 2020/11/9
	 * Time: 18:37
	 *
	 * @param array $data
	 * @return array
	 */
	public static function cartesianProduct(array $data) {
		$result = [];
		
		// 循环遍历集合数据
		$count = count($data);
		
		for ($i = 0; $i < $count - 1; $i++) {
			// 初始化
			if ($i == 0) {
				$result = $data[$i];
			}
			
			$tmp = [];
			
			// 结果与下一个集合计算笛卡尔积
			foreach ($result as $res) {
				foreach ($data[$i + 1] as $d) {
					$tmp[] = $res . $d;
				}
			}
			
			$result = $tmp;
		}
		
		return $result;
	}
	
	/**
	 * 数组获取白名单内的数据
	 *
	 * @param array $arr 源数组
	 * @param array $keys
	 * @return array
	 * @example
	 *                   $arr = ['a'=>1,'b'=>2,'c'=>3,'d'=>4]
	 *                   $keys = ['a','c'];
	 *                   $return = ['a'=>1,'c'=>3];
	 */
	public static function whiteList(array $arr, array $keys) {
		return array_intersect_key($arr, array_flip($keys));
	}
	
	/**
	 * 数组获取黑名单内的数据
	 *
	 * @param array $arr 源数组
	 * @param array $keys
	 * @return array
	 * @example
	 *                   $arr = ['a'=>1,'b'=>2,'c'=>3,'d'=>4]
	 *                   $keys = ['a','c'];
	 *                   $return = ['b'=>2,'d'=>4];
	 */
	public static function blackList(array $arr, array $keys) {
		return array_diff_key($arr, array_flip($keys));
	}
	
	/**
	 * 把索引数组转成按指定key的关联数组（重复会覆盖）
	 *
	 * @param array        $arr
	 * @param string|array $keyName
	 * @return array
	 */
	public static function toKeyArray(array $arr, $keyName) {
		$result = [];
		
		foreach ($arr as $i => $item) {
			if (!is_numeric($i)) {
				continue;
			}
			
			if (is_string($keyName)) {
				$key = '';
				if (is_array($item) && isset($item[$keyName]) && !empty($item[$keyName])) {
					$key = $item[$keyName];
				} elseif (is_object($item) && isset($item->$keyName) && !empty($item->$keyName)) {
					$key = $item->$keyName;
				}
				
				// 重复的将被后面覆盖
				// 对应的键值不能为空
				if (!empty($key)) {
					$result[$key] = $item;
				}
			} elseif (is_array($keyName)) {
				$keys = [];
				foreach ($keyName as $iKeyName => $itemKeyName) {
					$key = '';
					if (is_array($item) && isset($item[$keyName]) && !empty($item[$keyName])) {
						$key = $item[$keyName];
					} elseif (is_object($item) && isset($item->$keyName) && !empty($item->$keyName)) {
						$key = $item->$keyName;
					}
					
					// 注意每一层字段值都必须不能为空
					// 最好是可以作为主键的字段 不管他是不是主键 至少他有这个能力 要么就是保证不为空
					if (empty($key)) {
						$keys = [];
						break;
					}
					
					$keys[] = $key;
				}
				
				$res = &$result;
				foreach ($keys as $iKey => $itemKey) {
					if ($iKeyName == count($keyName) - 1) {
						// 最后一层
						$res[$itemKey] = $item;
					} else {
						!isset($res[$itemKey]) && $res[$itemKey] = [];
						$res = &$res[$itemKey];
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * 把索引数组转成按指定key的关联数组（无论是否重复都另建数组叠加）
	 *
	 * @param array        $arr
	 * @param string|array $keyName
	 * @return array
	 */
	public static function toKeyArrayOverlay(array $arr, $keyName) {
		$result = [];
		
		foreach ($arr as $i => $item) {
			if (!is_numeric($i)) {
				continue;
			}
			
			if (is_string($keyName)) {
				$key = '';
				if (is_array($item) && isset($item[$keyName]) && !empty($item[$keyName])) {
					$key = $item[$keyName];
				} elseif (is_object($item) && isset($item->$keyName) && !empty($item->$keyName)) {
					$key = $item->$keyName;
				}
				
				// 重复的叠加
				// 对应的键值不能为空
				if (!empty($key)) {
					$result[$key][] = $item;
				}
			} elseif (is_array($keyName)) {
				$keys = [];
				foreach ($keyName as $iKeyName => $itemKeyName) {
					$key = '';
					if (is_array($item) && isset($item[$keyName]) && !empty($item[$keyName])) {
						$key = $item[$keyName];
					} elseif (is_object($item) && isset($item->$keyName) && !empty($item->$keyName)) {
						$key = $item->$keyName;
					}
					
					// 注意每一层字段值都必须不能为空
					// 最好是可以作为主键的字段 不管他是不是主键 至少他有这个能力 要么就是保证不为空
					if (empty($key)) {
						$keys = [];
						break;
					}
					
					$keys[] = $key;
				}
				
				$res = &$result;
				foreach ($keys as $iKey => $itemKey) {
					if ($iKeyName == count($keyName) - 1) {
						// 最后一层
						$res[$itemKey][] = $item;
					} else {
						!isset($res[$itemKey]) && $res[$itemKey] = [];
						$res = &$res[$itemKey];
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * 链接数组
	 * 主要用于获取数据库数据列表之间链接
	 *
	 * Date: 2021/2/9
	 * Time: 17:12
	 *
	 * Master: [['a1' => 1, 'b1' => 2], ['a1' => 11, 'b1' => 3]]
	 * Slave1: [['aa1' => 1, 'bb1' => 22, ...], ['aa1' => 11, 'bb1' => 33, ...]]
	 * Slave2: [['aa2' => 11, 'bb2' => 44, ...], ['aa2' => 1, 'bb2' => 55, ...]]
	 *
	 * $ret = joinArrays([$master, 'a', ''], [[$slave1, 'aa1', 'kaa1'], [$slave2, 'aa2', 'kaa2']]
	 *
	 * $ret:
	 *  [
	 *      [
	 *          'a1' => 1,
	 *          'b1' => 2,
	 *          'kaa1' => ['aa1' => 1, 'bb1' => 22],
	 *          'kaa2' => ['aa2' => 1, 'bb2' => 55],
	 *      ],
	 *      [
	 *          'a1' => 11,
	 *          'b1' => 3,
	 *          'kaa1' => ['aa1' => 11, 'bb1' => 33],
	 *          'kaa2' => ['aa2' => 11, 'bb2' => 44],
	 *      ]
	 *  ]
	 *
	 * @param array $arrMaster 主数据[data, field, key] data-数据数组 field-作为链接的数据字段 key-返回结果中自定义键（可以留空）
	 * @param array $arrSlave 从数据[[data, field, key], [...]] key-在从数据中不能留空
	 * @return array
	 */
	public static function joinArrays(array $arrMaster, array $arrSlave) {
		$result = [];
		
		// 将从数据列表的索引数组转换为key数组
		$arrSlaveKey = [];
		foreach ($arrSlave as $row) {
			// $row[0]: data; $row[1]: field; $row[2]: key
			$arrSlaveKey[] = [
				'data' => self::toKeyArrayOverlay($row[0], $row[1]),
				'key' => $row[2],
			];
		}
		
		unset($arrSlave);
		$arrSlave = null;
		
		// 主数据处理及从数据链接
		foreach ($arrMaster as $row) {
			$_data = [];
			// $row[0]: data; $row[1]: field; $row[2]: key
			if (!empty($row[2])) {
				$_data[$row[2]] = $row[0];
			} else {
				$_data = $row[0];
			}
			
			// 链接从数据
			foreach ($arrSlaveKey as $item) {
				$_data[$item['key']] = $item['data'][$row[1]] ?? [];
			}
			
			$result[] = $_data;
		}
		
		return $result;
	}
	
}